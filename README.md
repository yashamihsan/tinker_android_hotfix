# Tinker android Hot-fix 
tinker_android_hotfix example app
Android Hotfix demo app using Tinker Hotfix solution. we can fix bug, update dex, library and resources without reinstall apk.
Based on config api call
then download patch file from backend server and then apply the patch.

Config mock API response model:
![](config_response.PNG)

Before case- when app has a bug, null exception.

![](20191110_025058.gif)

After Hotfix Patch Applied - Bug Fixed 

![](20191110_024810.gif)

# Library Used:
[Tinker Hot-fix by Tencent](https://github.com/Tencent/tinker)
